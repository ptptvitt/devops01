from flask import Flask


app = Flask(__name__)


@app.route("/")
def index():
    app.logger.info("Index route accessed.")
    return "Hello Pymi devops"


if __name__ == "__main__":
    app.run(debug=True)
